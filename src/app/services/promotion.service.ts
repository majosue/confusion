import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { baseURL } from '../shared/baseurl';
//import { delay } from 'rxjs/operators';
import { Promotion } from '../shared/promotion';
//import { PROMOTIONS } from '../shared/promotions';
import { ProcessHTTPMsgService} from '../services/process-httpmsg.service';


@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private http: HttpClient,
    private procwssHTTPMsgService: ProcessHTTPMsgService) { }

    getPromotions(): Observable<Promotion[]> {
      return this.http.get<Promotion[]>(baseURL + 'promotions')
        .pipe(catchError(this.procwssHTTPMsgService.handleError));
    }

    getPromotion(id: string): Observable<Promotion> {
      return this.http.get<Promotion>(baseURL + 'promotions/' + id);
    }

    getFeaturedPromotion(): Observable<Promotion> {
      return this.http.get<Promotion>(baseURL + 'promotions?featured=true')
      .pipe(map(promotions => promotions[0]))
      .pipe(catchError(this.procwssHTTPMsgService.handleError));
    }

}
